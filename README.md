# fluxcd

## Prepare

1. Install Rancher and configure Cluster

```
docker run -d --restart=unless-stopped -p 8080:80 -p 8443:443 rancher/rancher:stable
```

2. Add CRD

```bash
kubectl apply -f https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/crds.yaml
```

3. Add Fluxcd Repo

```
helm repo add fluxcd https://charts.fluxcd.io
```

4. Deploy Helm-Operator

```yaml
helm:
  versions: "v3"
```

Repo: https://github.com/fluxcd/helm-operator/tree/master/chart/helm-operator

5. Deploy Flux

```yaml
git:
  url: "https://gitlab.com/ZzenlD/argo.git"
  branch: "master"
  path: "prod"
  readonly: true
  pollInterval: "1m"

prometheus:
  enabled: true

dashboards:
  enabled: true
  namespace: prometheus-prod
```

Repo: https://github.com/fluxcd/flux/tree/master/chart/flux
